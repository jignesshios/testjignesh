//
//  ContentView.swift
//  JIgneshTest
//
//  Created by jignesh kalantri on 10/05/23.
//

import SwiftUI

struct ContentView: View {
    let names = ["Holly", "Josh", "Rhonda", "Ted"]
    
    let stories = ["danny", "demarc", "dray", "danny"]

    @State private var searchText = ""
    @State private var index = 0

    var body: some View {
        NavigationStack {
            TabView(selection: $index) {
                          ForEach((0..<3), id: \.self) { index in
                              CardView(name: stories[index])
                          }
                      }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
//            ScrollView(.horizontal, showsIndicators: false) {
//                HStack(spacing: 10) {
//                    ForEach((0..<3), id: \.self) { index in
//                        Image(stories[index])
//                            .resizable()
//                            .aspectRatio(contentMode:.fill)
//                            .frame(width: UIScreen.main.bounds.width - 32, height: 180)
//                            .border(.black)
//                    }
//                }.padding()
//            }
            VStack {
                HStack(spacing: 2) {
                    ForEach((0..<3), id: \.self) { index in
                        Circle()
                            .fill(index == self.index ? Color.purple : Color.purple.opacity(0.5))
                            .frame(width: 20, height: 20)

                    }
                }
            }
            
            List {
                ForEach(searchResults, id: \.self) { name in
                        HStack {
                            Image("tr")
                                .resizable()
                                .frame(width: 25, height: 25)
                            Text(name)
                        }
                }
            }
            .listStyle(.plain)
        }
        .searchable(text: $searchText) {
            ForEach(searchResults, id: \.self) { result in
                Text("\(result)")
            }
        }
    }

    var searchResults: [String] {
        if searchText.isEmpty {
            return names
        } else {
            return names.filter { $0.contains(searchText) }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
