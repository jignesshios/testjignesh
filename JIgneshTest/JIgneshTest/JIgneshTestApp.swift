//
//  JIgneshTestApp.swift
//  JIgneshTest
//
//  Created by jignesh kalantri on 10/05/23.
//

import SwiftUI

@main
struct JIgneshTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
