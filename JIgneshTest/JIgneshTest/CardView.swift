//
//  CardView.swift
//  illaExamiOSJignesh
//
//  Created by jignesh kalantri on 09/05/23.
//

import SwiftUI

struct CardView: View{
    var name : String
    var body: some View{
             Image(name)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: UIScreen.main.bounds.width - 35, height: 190)
            .border(Color.black)
            .padding()
    }
}
